#!/bin/bash -l
################################################################################
#
# File:        fdmnes.sh
# Description: Script to run the FDMNES program at the ESRF. The command line
#              arguments allow to select a parallel version of the program and
#              to specify an input file for FDMNES.
#
#              The script can also be used to run FDMNES with the OAR scheduler.
#
#              Usage: fdmnes.sh [-h] [-np nproc] [-f hostfile] [FDMNES_file]
#              Further, an environment variable HOST_NUM_FOR_MUMPS can be used.
#
#              At the ESRF, FDMNES is available for 64 bit LINUX in both a
#              parallel MPI and a sequential version. Only the sequential
#              version is available for Windows with Cygwin. The script
#              automatically selects the correct version for the operating
#              system and hardware used.
#
#              There are two levels of parallelisation possible in FDMNES. The
#              program calculates X-ray spectra for a user-defined range of
#              energies. The calculation of several energies can happen in
#              parallel, that is one level of FDMNES parallelisation. Each
#              energy calculation involves the solution of a sparse matrix
#              equation, which in turn can be parallelized.
#
#              The energy calculations can require rather large amounts of
#              memory, and it is often not possible to run as many processes on
#              the nodes as there are cores available because there is not
#              enough memory to accomodate them. The script offers a possibility
#              to define a maximum number of parallel processes per node.
#
#              The number of processes to run can be selected by the user with
#              the "-np" option and the environment variable HOST_NUM_FOR_MUMPS.
#              The "nproc" argument of "-np" specifies the number of parallel
#              energy processes to use. HOST_NUM_FOR_MUMPS sets the number of
#              parallel matrix calculations per energy (default: 1).
#
#              The total number of parallel processes to run is therefore
#                 ntot = (# of energy processes) * HOST_NUM_FOR_MUMPS
#
#              FDMNES scales rather well with the number of parallel processes
#              in the energy loop, but for the matrix calculation it goes into
#              saturation at about 4 parallel matrix processes per energy.
#
#              If the "-np" option is not given, on the "rnice" computers the
#              total number of processes is set to "1", on the "OAR" cluster it
#              is determined from the OAR nodefile.
#
#              This means in particular if one wants to run only one parallel
#              energy process but with several parallel matrix calculations,
#              "-np" has to be specified on the command line as "-np 1".
#
#              On the "rnice" computers, the total number of processes together
#              with the variable NPERNODE (set in this script) is used to
#              calculate the number of nodes needed for the job.
#
#              On the OAR cluster, normally the total number of processes is
#              equal to the number of cores available for the job. If, however,
#              the "-np" parameter is specified, then the corresponding total
#              number of parallel processes together with the number of nodes is
#              used to calculate the total number of parallel processes per
#              node (energy * matrix). This allows to undersubscribe the nodes,
#              in particular to have more memory available per process.
#
#              If the total number of processes to run is "1", the sequential
#              version of FDMNES will be used.
#
#              The "FDMNES_file" parameter contains the name of the input file
#              for FDMNES. This file will be copied to the standard FDMNES input
#              file (which has a fixed name, see variable FDMINFIL below). If
#              this parameter has not been specified, the FDMNES default name
#              in FDMINFIL is used.
#
#              If the parallel version is used, it needs (in addition to the
#              FDMNES input file) two configuration files:
#              - a password file
#              - a host file with the names of the machines to use for MPI.
#
#              The password file must be "$(HOME)/.mpd.conf", otherwise the
#              script aborts with an error message.
#
#              The host file by default has the name "mpd.hosts" and is in the
#              directory where the script is started, but this can be overridden
#              with the "-f" option of the script.
#
#              For more details about these configuration files, see the 
#              documentation for OpenMPI.
#
#              If the script is called from the OAR scheduler (interactively or
#              in a batch job), the name of the host file is taken from the OAR
#              environment. Any corresponding specification in the script's
#              parameters is ignored.
#
################################################################################
# Created:     23/11/2005 R. Wilcke (wilcke@esrf.fr)
#
# Modified:    16/01/2020 R. Wilcke (wilcke@esrf.fr)
#              added variable FDMVERS containing the desired version of FDMNES.
# Modified:    18/12/2019 R. Wilcke (wilcke@esrf.fr)
#              updated description of functionality.
# Modified:    12/12/2019 R. Wilcke (wilcke@esrf.fr)
#              remove macros SYS32 and SYS64 (no longer used).
# Modified:    03/11/2019 R. Wilcke (wilcke@esrf.fr)
#              - use "bash" with option "-l" to have command "module" available;
#              - use "module" commands to get the default version of FDMNES, or
#                of MUMPS if a different version of FDMNES is used;
#              - change MPIHOME to MPI_HOME, this is defined in the MPI modules.
# Modified:    25/07/2019 R. Wilcke (wilcke@esrf.fr)
#              replaced "/bin/sh" by "/bin/bash" as shell to use;
#              get the FDMHOME directory from the BASH_SOURCE array variable;
#              remove FDMVERS variable (no longer needed).
# Modified:    20/06/2019 R. Wilcke (wilcke@esrf.fr)
#              - introduce new parameter FDMVERS to define the FDMNES home
#                directory FDMHOME;
#              - include dummy string OPSYS in parameter FDMHOME;
#              - add code to later replace dummy OPSYS by the real name of the
#                operating system before using $FDMHOME to set the executable.
# Modified:    04/04/2019 R. Wilcke (wilcke@esrf.fr)
#              changed MPIHOME to the directory for OpenMPI 3.1.3.
# Modified:    05/12/2018 R. Wilcke (wilcke@esrf.fr)
#              renamed NPERHOST to NPERNODE;
#              restructured the setting of the "processes per node" parameter,
#              using also the FDMNES environment variable HOST_NUM_FOR_MUMPS;
#              changed MPIHOME to the directory for OpenMPI 3.1.3;
#              replace the MPI "mca btl" parameter value "sm" by "vader".
# Modified:    20/06/2017 R. Wilcke (wilcke@esrf.fr)
#              changed MPIHOME to the directory for OpenMPI 2.0.2;
#              added setting of LD_LIBRARY_PATH for OpenMPI libraries.
# Modified:    08/07/2016 R. Wilcke (wilcke@esrf.fr)
#              exclude also "docker0" and (for IB only) "eth1" from MPI devices.
# Modified:    07/12/2015 R. Wilcke (wilcke@esrf.fr)
#              select Ethernet interface for OAR if not all nodes are on the
#              same IB switch.
# Modified:    04/12/2015 R. Wilcke (wilcke@esrf.fr)
#              correct selection of network interface for OAR (Ethernet or
#              Infiniband).
# Modified:    26/11/2015 R. Wilcke (wilcke@esrf.fr)
#              correct use of the "plm_rsh_agent" for OAR and non-OAR.
# Modified:    21/08/2015 R. Wilcke (wilcke@esrf.fr)
#              extensive modification to make script work with OpenMPI instead
#              of Intel MPI.
# Modified:    28/11/2013 R. Wilcke (wilcke@esrf.fr)
#              add comment about the number of hosts for OAR.
# Modified:    15/11/2013 R. Wilcke (wilcke@esrf.fr)
#              changed MPIHOME to the new path for Intel MPI version 4.1.0.024.
# Modified:    11/09/2013 R. Wilcke (wilcke@esrf.fr)
#              add macro NPERHOST for the maximum number of processes per host;
#              use NPERHOST and "nproc" to calculate the number of hosts needed.
# Modified:    22/05/2013 R. Wilcke (wilcke@esrf.fr)
#              changed MPIHOME to the new path for Intel MPI version 4.0.3.008;
#              add macro "exe" for the ".exe" extension of executables under
#              CYGWIN;
#              add separate test for CYGWIN in determination of OS.
# Modified:    27/11/2012 R. Wilcke (wilcke@esrf.fr)
#              replace "I_MPI_DEVICE sock" by "I_MPI_FABRICS shm:tcp";
#              replace "-perhost 1" by "-perhost 2".
# Modified:    06/04/2011 R. Wilcke (wilcke@esrf.fr)
#              modified script to work with the OAR scheduler.
# Modified:    28/03/2011 R. Wilcke (wilcke@esrf.fr)
#              changed MPIHOME to the new path for Intel MPI version 4.0.1.007.
# Modified:    11/03/2011 R. Wilcke (wilcke@esrf.fr)
#              added option "-genv I_MPI_DEVICE sock" for MPI on "rnice".
# Modified:    15/05/2009 R. Wilcke (wilcke@esrf.fr)
#              changed path MPIHOME to the new path for Intel MPI version 3.1.1
# Modified:    20/01/2009 R. Wilcke (wilcke@esrf.fr)
#              in check for the processor parameter, put backslash escape ("\")
#              in front of first RE to prevent possible expansion as file name.
# Modified:    07/08/2007 R. Wilcke (wilcke@esrf.fr)
#              modified location of the script for the MPI environment
#              variables.
# Modified:    15/06/2007 R. Wilcke (wilcke@esrf.fr)
#              changed code generalize the available operating systems and
#              distinguish automatically between 32 and 64 bit architectures.
# Modified:    30/03/2007 R. Wilcke (wilcke@esrf.fr)
#              corrected name and location of the script for the MPI environment
#              variables.
# Modified:    02/02/2007 R. Wilcke (wilcke@esrf.fr)
#              define function "err_exit" for error exits;
#              removed SunOS from the script;
#              distinguish for 32 bit Linux between Suse and Redhat;
#              use CS "get_os" to determine the operating system;
#              make changes to adapt script for MPI2;
#              add "-h" and "-f" command line parameters.
# Modified:    14/12/2005 R. Wilcke (wilcke@esrf.fr)
#              changed operator "==" to "=" for string test;
#              replaced the "length" operator in the "expr" command by a match
#              for all characters ("length" gave problems on the SUN);
#              test for NULL string instead of string length 0 before execute.
#
# Language:    Bourne shell script with indented text
# Package:     FDMNES
# Status:      Operational
#
# (C) Copyright 2005, Scientific Software Group, ESRF, all rights reserved.
################################################################################
#
# Parameters to be reconfigured if used on a private or beamline computer:
#
# - FDMVERS:  the version of FDMNES to be used (form: yyyymmdd);
# - FDMHOME:  the FDMNES home directory;
# - FDMINFIL: the standard name of the input file for the FDMNES program.
#
# Additionally if MPI is to be used:
# - for the "rnice" computers:
#   -- HOSTFILE: the file with the host node names;
#   -- NPERNODE: the maximum number of processes per host.
#      It should be less than the number of cores per host, because otherwise
#      performance can be severely degraded if there is at least one other
#      compute-intensive job on the same node.
#
# On OAR, the two last parameters are determined from the OAR environment.
#
FDMVERS=20200831
FDMHOME=/scisoft/users/wilcke/swdev/fdmnes/fdmnes_$FDMVERS/debian8/bin
FDMINFIL=fdmfile.txt
HOSTFILE=mpd.hosts
NPERNODE=2

if [ X$FDMHOME = X ]
then
   module load fdmnes/$FDMVERS
else
   FDMHOME=${FDMHOME}/
   module load mumps/5.1.2
fi

#MPIDEBUG="--verbose"
#
# Help message how to use the script.
#
usage()
{
   echo Usage: `basename "$0"` [-h] [-np nproc] [-f hostfile] [FDMNES_file]
   exit 1
}
#
# Error exit function.
#
err_exit()
{
   echo "ERROR: $1"
   exit 1
}
#
# Determine operating system. Normally this is done with the CS script "get_os".
# If this fails, the user must define the operating system manually.
#
# The case "Cygwin" is treated separately, because "get_os" may not be installed
# on the corresponding PC.
#
# The possible values for the operating system are listed in the description at
# the beginning of the script.
#
if [ `uname -o` = "Cygwin" ]
then
   OPSYS=cygwin
   exe=.exe
else
   export PATH=$PATH:/csadmin/common/scripts
   OPSYS=`get_os`
   if [ $? -ne 0 ]
   then
      err_exit "Cannot determine operating system  - define manually in script"
   fi
fi
#
# Test for correct usage: can be called with up to 6 parameters.
#
if [ ! $# -le 6 ]
then
   usage
fi
#
# Preset FDMNES input file name.
#
infile=$FDMINFIL
#
# Loop over the command line parameters.
#
while [ $# -gt 0 ]
do
   case "$1" in
#
# Help message
#
   -h)
      usage
      ;;
#
# If the MPI version is to be used, check if the process parameter is specified
# correctly (it must be a number and it must be bigger than 0).
# (backslash to prevent shell expansion as file name for files like "10.dat".)
#
   -np)
      nproc=$2
      if [ `expr $nproc : \[1-9][[:digit:]]*` -ne `expr $nproc : '.*'` ]
      then
         err_exit "illegal number of processes requested: \"$nproc\""
      fi
      shift 2
      ;;
#
# Set name of the MPI host file.
#
   -f)
      HOSTFILE=$2
      shift 2
      ;;
#
# Any other parameter starting with a "-" is an error.
#
   -*)
      usage
      ;;
#
# A parameter not starting with a "-" is either an argument to the other options
# and already dealt with above, or it can be the FDMNES input file name. In this
# case it must be the last argument.
#
   *)
      if [ $# -eq 1 ]
      then
         infile=$1
      else
         usage
      fi
      shift
      ;;
   esac
done
#
# Set the environment variable HOST_NUM_FOR_MUMPS to 1 if it is not set.
#
if [ X$HOST_NUM_FOR_MUMPS = X ]
then
   export HOST_NUM_FOR_MUMPS=1
fi
#
# Find out if the script has been called from the "OAR" scheduling environment.
#
if [ X$OAR_JOB_ID = X ]
then
#
# On rnice, if the number of processes has not been specified, set it to 1.
# If it has been specified, it is the number of parallel energy processes.
# Recalculate the real number of processes taking into account the number of  
# parallel processes for the matrix calculation.
#
   FROMOAR=0
   if [ X$nproc = X ]
   then
      nproc=1
   else
      nproc=`expr $HOST_NUM_FOR_MUMPS \* $nproc`
   fi
else
#
# If on OAR:
# - HOSTFILE is set to the OAR variable OAR_NODEFILE.
# - the number of hosts is the number of different nodes in the OAR_NODEFILE.
# - if the number of processes has not been specified on the command line, set
#   it to the number of entries in the OAR_NODEFILE. That means that on each
#   node there will be as many processes as there are cores.
# - if it has been specified, use it to calculate the number of parallel energy
#   processes per node. This allows to "undersubscribe" the nodes: some cores
#   will remain idle to give the running processes more resources (in particular
#   memory). The total number of processes is then the number of energy
#   processes times the number of matrix processes.
#
   FROMOAR=1
   HOSTFILE=$OAR_NODEFILE
   ncores=`wc -l $OAR_NODEFILE | awk '{print $1}'`
   nhosts=`uniq $OAR_NODEFILE | wc -l`
   if [ X$nproc = X ]
   then
      nproc=$ncores
   else
      nproc=`expr $HOST_NUM_FOR_MUMPS \* $nproc`
      if [ $nproc -gt $ncores ]
      then
         err_exit "total # processes \"$nproc\" larger than # cores \"$ncores\""
      fi
      npern=`expr $nproc / $nhosts`
      if [ $nproc -ne `expr $nhosts \* $npern` ]
      then
         err_exit "# processes \"$nproc\" not multiple of # hosts \"$nhosts\""
      fi
   fi
fi
#
# If there is more than 1 process to start, use the MPI version of the program.
#
if [ $nproc -gt 1 ]
then
   mpi=_mpi
fi
#
# Test if the specified input file exists. If not, exit with error.
#
if [ ! -r "$infile" ]
then
   err_exit "file \"$infile\" not existent or not readable"
fi
#
# Test if the specified input file name is the standard FDMNES input file name.
# (contained in variable FDMINFIL).
#
# If not, copy the input file to the standard FDMNES input file.
# Exit with error if the copy does not succeed.
#
if [ "$infile" != "$FDMINFIL" ]
then
   cp $infile $FDMINFIL
   if [ $? -ne 0 ]
   then
      err_exit "cannot copy file \"$infile\" to \"$FDMINFIL\""
   fi
fi
#
# Get the executable.
#
execut=$(which ${FDMHOME}fdmnes$mpi$exe)
if [ ! -x $execut ]
then
   err_exit "no FDMNES executable available for operating system \"$OPSYS\""
fi
#
# For non-MPI jobs, "exec" the new process.
#
if [ -z $mpi ]
then
   CMD="exec $execut"
   $CMD
#
# For MPI jobs:
# - check if the password file can be found and read. Exit with error if not;
# - prepare the MPI environment variables for this operating system;
# - test if the required number of hosts is available. Exit with error if not;
# - finally run the executable with "mpiexec".
#
else
   if [ ! -r $HOME/.mpd.conf ]
   then
      err_exit "password file \"$HOME/.mpd.conf\" not existent or not readable"
   fi
#
# Prepare the arguments for the MPI parallel running.
#
   if [ $FROMOAR -eq 0 ]
   then
#
# MPI command arguments if running interactively without OAR:
# - check if the hostfile can be found and read. Exit with error if not;
# - set the "npernode" parameter to the value of NPERNODE;
# - set the HOST_NUM_FOR_MUMPS environment variable for the MPI run environment;
# - set the "orte_rsh_agent" MCA parameter to use "ssh" as remote shell;
# - set the "tcp" device parameter because there is no Infiniband available.
#
      if [ ! -r $HOSTFILE ]
      then
         err_exit "host file \"$HOSTFILE\" not existent or not readable"
      fi
      MPIPARAM="--npernode $NPERNODE -x HOST_NUM_FOR_MUMPS=$HOST_NUM_FOR_MUMPS"
      MPIPARAM="$MPIPARAM --mca plm_rsh_agent ssh -np $nproc $execut"
      MPIDEV="--mca btl tcp,vader,self"
   else
#
# On the OAR cluster:
# - set the MPI parameter "--npernode" for the number of processes per node if
#   it should be less than the number of cores available per node;
# - set the HOST_NUM_FOR_MUMPS environment variable for the MPI run environment;
# - set the "orte_rsh_agent" MCA parameter to use "oarsh" as remote shell;
# - specify the number of processes to run;
# - find out if all hosts have Infiniband (IB) interface and set the device
#   parameter accordingly.
#
      if [ X$npern != X ]
      then
         MPIPARAM="--npernode $npern"
      fi
      MPIPARAM="$MPIPARAM -x HOST_NUM_FOR_MUMPS=$HOST_NUM_FOR_MUMPS"
      MPIPARAM="$MPIPARAM --mca plm_rsh_agent oarsh -np $nproc $execut"
#
# Test if at least one of the hosts does not have an InfiniBand (IB) interface.
# If so, set the device parameter to include "tcp" and not "openib", otherwise
# the other way round.
#
# Get the list of hosts.
#
      hstlst=$(oarprint host -P host)
#
# Loop over all hosts in the list
#
      MPIDEV="--mca btl openib,vader,self \
              --mca btl_tcp_if_exclude ib0,lo,eth0,eth1,docker0"
      ibswtold='NA'
      for i in $hstlst
      do
         hostname=$i
#
# Test if the host has an IB interface. Set the "tcp" device parameter if at
# least one host does not have IB.
#
# If the host has an IB interface, test if all hosts have the same interface.
# Set the "tcp" device parameter if not, as the two IB interfaces available
# cannot communicate with each other.
#
         ibdev=$(oarprint host -P host,ib | grep $hostname | cut -d ' ' -f 2-)
         if [ $ibdev = 'NO' ]
         then
            MPIDEV="--mca btl tcp,vader,self \
                    --mca btl_tcp_if_exclude ib0,lo,docker0"
            break
         else
            ibswtnew=$(oarprint host -P host,ibswitch | grep $hostname | \
               cut -d ' ' -f 2-)
            if [ $ibswtold = 'NA' ]
            then
               ibswtold=$ibswtnew
               continue
            elif [ $ibswtold != $ibswtnew ]
            then
               MPIDEV="--mca btl tcp,vader,self \
                       --mca btl_tcp_if_exclude ib0,lo,docker0"
               break
            fi
         fi
      done
   fi
#
# Run the MPI process with "orterun", including all the options.
#
# For this, set the LD_LIBRARY_PATH environment variable for the MPI command to
# pick up the OpenMPI libraries. The expression tests whether the variable is
# already set or not (it adds ":" if it is set).
#
# Note: in OpenMPI, "orterun", "mpirun" and "mpiexec" are all identical. But as
# Intel's MPI also has the commands "mpirun" and "mpiexec", "orterun" is used
# here to avoid using the wrong command.
#
   CMD="$MPI_HOME/bin/orterun -hostfile $HOSTFILE $MPIDEBUG $MPIDEV $MPIPARAM"
   echo $CMD
   LD_LIBRARY_PATH=$MPI_HOME/lib${LD_LIBRARY_PATH:+:}$LD_LIBRARY_PATH
   export LD_LIBRARY_PATH; $CMD
fi
