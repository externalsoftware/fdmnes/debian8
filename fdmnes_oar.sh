#!/bin/bash -l
################################################################################
#
# Example of a command file to submit FDMNES jobs to the OAR batch queue.
#
# The FDMNES program calculates X-ray spectra over a user-defined range of
# energies. There is a serial and a parallel version available. The parallel
# version will automatically be selected if the user requests more than one
# process to run when submitting the job to the OAR batch queue.
#
# FDMNES has two levels of parallelisation. The main one is over the energies,
# and this scales well with the number of cores assigned to the job. The second
# one is a sparse matrix calculation that can be parallelised for every energy
# calculation. This scales well only up to about 4 parallel processes per
# energy.
#
# If the FDMNES script that is submitted by this command file is called without
# arguments concerning the parallel processes, the default is that the matrix
# calculation is not parallelised and that the number of cores requested from
# the OAR scheduler is the number of parallel processes for the energy.
#
# FDMNES calculations may need large amounts of memory, and assigning that many
# parallel processes to the nodes can cause the job to be terminated for
# insufficient memory. It is possible to limit the number of parallel energy
# processes with the "-np" parameter to the script.
#
# If only that parameter is specified for the calculation, there is still no
# parallelisation for the matrix calculations. This can be enabled by setting
# the environment variable HOST_NUM_FOR_MUMPS. The value of this variable gives
# the number of parallel matrix calculations for each energy point. The default
# is 1, i.e. no parallelisation for the matrix calculations.
#
# For a given job, the total number of parallel processes is thus
#    ntot = (# of energy processes) * HOST_NUM_FOR_MUMPS
# where the number of energy processes is specified by the "-np" parameter.
#
# For more details, see the script "fdmnes.sh", the file "Readme_ESRF.txt" in
# the FDMNES home directory at the ESRF, and the documentation in the FDMNES
# distribution.
#
# The batch jobs are submitted to OAR with the "oarsub" command. Each "oarsub"
# command submits one job. If several jobs are to be submitted, just give the
# corresponding number of "oarsub" commands (see examples below).
#
# The "oarsub" command can take many options (for details, see "man oarsub" or
# the OAR documentation in http://oar.imag.fr/users/user_documentation.html).
#
# The ones most useful for FDMNES are:
# - the number of cores to use (default: 1);
# - the maximum (elapsed) run time (default: 2 hours);
# - the directories where the job starts (default: the present directory).
#
# Other options that can be useful are the memory available (for jobs that need
# large quantities of memory) and the type of network interconnect (Infiniband
# for jobs that need fast interprocess communication).
#
# The number of cores and the maximum run time are specified with the "oarsub"
# option "-l" in the form
#    -l /core=ncore,walltime=nh:nm:ns
# with
#    ncore: the number of cores
#    nh   : the number of hours to run
#    nm   : the number of minutes to run
#    ns   : the number of seconds to run
#
# Example: -l /core=4,walltime=1:20:00
#    requests 4 cores and a total elapsed time of 1 hour 20 minutes.
#
# If one wants to submit more than one job with this command file, they have to
# be launched in different directories, as FDMNES will only accept the fixed
# name "fdmfile.txt" as its input file name. The "oarsub" option "-d" allows to
# specify for each job a directory (with path!) where the job will be started.
#
# Example: -d /users/dupont/data/exp1234
#    starts the FDMNES batch job in the directory "/users/dupont/data/exp1234".
#
# Memory and network interconnect options are specified with the property option
# "-p". For memory, one can request memory per node or per core with the
# parameters "mem_total_mb" or "mem_core_mb". For network interconnect, the
# parameter is "ib" with the possible values 'YES' and 'NO'.
#
# Example: -p "mem_total_mb>=258000 and ib='YES'"
#    starts the FDMNES batch job on nodes with at least 250000 MB and Infiniband
#
# If one wants to give input arguments (like the number of parallel energy
# processes) to the executable, then the name of the executable and the input
# arguments must be enclosed in quotes (").
#
# Example: -l nodes=8/core=28,walltime=72 "$EXECUT -np 32"
#
#    starts the FDMNES batch job on 8 nodes with 28 cores each with a total
#    elapsed time of 72 hours. The executable is given the additional input
#    argument "-np 32", which means that only 32 parallel energy loops will be
#    started: 4 per node, which leaves 24 cores per node idle. This can be used
#    to give more memory to each energy process.
#
# If one wants to parallelise the sparse matrix calculation for each energy
# point, the environment variable HOST_NUM_FOR_MUMPS has to be set to a value
# different than 1. To do this, it should be specified on the command line with
# the executable, and the whole command must be enclosed in quotes (").
#
# Example: -l nodes=3/core=28,walltime=72 "export HOST_NUM_FOR_MUMPS=4; $EXECUT"
#
#    starts the FDMNES batch job on 3 nodes with 28 cores each with a total
#    elapsed time of 72 hours. 84 parallel processes will be started (3 nodes
#    with 28 cores each), but each energy process executes the matrix
#    calculation on 4 parallel processes as requested by the HOST_NUM_FOR MUMPS
#    environment variable. Thus there will be only 21 parallel energy processes
#    running (7 per node).
#
# The specification for the number of parallel energy processes and the one for
# the number of parallel matrix calculations can both be given for the same job.
#
# Example:
#   -l nodes=3/core=28,walltime=72 "export HOST_NUM_FOR_MUMPS=4; $EXECUT -np 6"
#
#    starts the FDMNES batch job on 3 nodes with 28 cores each with a total
#    elapsed time of 72 hours. 6 parallel energy processes will be started as
#    specified by the "-np" parameter (3 on each node), but each energy process
#    executes the matrix calculation on 4 parallel processes as requested by the
#    HOST_NUM_FOR MUMPS environment variable. Thus there will be in total 24  
#    parallel processes running (8 per node).
#
################################################################################
# Created:     06/04/2011 R. Wilcke (wilcke@esrf.fr)
#
# Modified:    16/01/2020 R. Wilcke (wilcke@esrf.fr)
#              added variable FDMVERS containing the desired version of FDMNES.
# Modified:    26/06/2019 R. Wilcke (wilcke@esrf.fr)
#              get directory with script "fdmnes.sh" with "module" command;
#              use "bash login shell" (/bin/bash -l), needed for "module";
#              exit with error if script not found;
#              remove variable FDMVERS.
# Modified:    20/06/2019 R. Wilcke (wilcke@esrf.fr)
#              added variable FDMVERS to define the FDMNES version.
# Modified:    05/12/2018 R. Wilcke (wilcke@esrf.fr)
#              added "Example 5" that uses input arguments and the environment
#              variable HOST_NUM_FOR_MUMPS with the executable.
# Language:    Bourne shell script with indented text
# Package:     FDMNES
# Status:      Operational
#
# (C) Copyright 2011, DAU Unit, Software Group, ESRF, all rights reserved.
################################################################################

#
# Define the version of FDMNES to be used (form: yyyymmdd) and the directory
# containing the script "fdmnes.sh".
#
# Normally this directory is put in the PATH variable with the "module load"
# command, and the variable FDMHOME stays empty. If a different version of
# FDMNES is required, the corresponding "fdmnes.sh" script can be selected here.
#
FDMVERS=20200831
#FDMHOME=/scisoft/users/wilcke/swdev/fdmnes/fdmnes_$FDMVERS/debian8/bin

if [ X$FDMHOME = X ]
then
   module load fdmnes/$FDMVERS
else
   FDMHOME=${FDMHOME}/
fi

#
# The FDMNES jobs are started with the interface script "fdmnes.sh".
# Exit with error message if the script is not found.
#
EXECUT=`which ${FDMHOME}fdmnes.sh`
if [ $? -ne 0 ]
then
   echo "ERROR: script \"fdmnes.sh\" not found"
   exit 1
fi

#
# All examples are commented out. To select one, remove the comment symbols "#"
# on the corresponding "oarsub" line(s).
#

#
# Example 1: submit one non-parallel job with 30 minutes maximum runtime.
#
# No "core" resource argument is given, thus the default "1 core" is taken.
#
# The job will be started in the current directory. This must contain the FDMNES
# input files (in particular "fdmfile.txt"), and the output files will be
# created there as well.
#
#oarsub -l walltime=0:30:0 $EXECUT

#
# Example 2: submit one parallel job to 8 cores with 2 hours maximum runtime.
#
# No "walltime" resource argument is given, thus the default "2 hours" is taken.
#
# The job will be started in the current directory. This must contain the FDMNES
# input files (in particular "fdmfile.txt"), and the output files will be
# created there as well.
#
#oarsub -l /core=8 $EXECUT

#
# Example 3: submit two parallel jobs, each to 5 cores with 1 hour 30 minutes
# maximum runtime.
#
# Each job will be started in its own directory. This must contain the FDMNES
# input files (in particular "fdmfile.txt"), and the output files will be
# created there as well.
#
#oarsub -d /users/johndoe/example_1 -l /core=5,walltime=1:30:0 $EXECUT
#oarsub -d /users/johndoe/example_2 -l /core=5,walltime=1:30:0 $EXECUT

#
# Example 4: submit one parallel job to 4 nodes with 4 cores each with 1 hour
# maximum runtime. The nodes must have more than 258 000 MB memory each and be
# connected by Infiniband.
#
# The job will be started in the current directory. This must contain the FDMNES
# input files (in particular "fdmfile.txt"), and the output files will be
# created there as well.
#
#oarsub -l nodes=4/core=4,walltime=1:0:0 \
#   -p "mem_total_mb>=258000 and ib='YES'" $EXECUT

#
# Example 5: submit one parallel job to 3 nodes with 28 cores each for 72 hours
# maximum runtime. For this job, both the number of parallel energy processes
# and the number of parallel matrix calculations is specified.
#
# The executable contained in $EXECUT is given the additional input argument
# "-np 6" to specify that the job should be executed by 6 parallel processes for
# the energy loop; this means that there will only be 2 processes for the energy
# loop per node.
#
# However, each of those energy loops will use 4 parallel processes for the
# matrix calculation. That is specified by the HOST_NUM_FOR_MUMPS environment
# variable. Thus there will be 8 parallel processes per node.
#
# The total number of parallel processes for the OAR job is therefore 24.
#
# To specify this, the variable EXECUT containing the executable, the input
# argument and the definition of the HOST_NUM_FOR_MUMPS environment variable
# must all be enclosed together in one pair of quotes (").
#
# The job will be started in the current directory. This must contain the FDMNES
# input files (in particular "fdmfile.txt"), and the output files will be
# created there as well.
#
#oarsub -l nodes=3/core=28,walltime=72 \
#   "export HOST_NUM_FOR_MUMPS=4; $EXECUT -np 6"
oarsub -l nodes=2/core=8,walltime=1 \
   "export HOST_NUM_FOR_MUMPS=4; $EXECUT -np 4"
